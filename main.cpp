#include <iostream>
#include <string>
#include <fstream>
#include "BinarySearchTree.h"

using namespace std;

template<class ItemType>
void fileToTree(BinarySearchTree<ItemType> & temp, string filename);
void visit(string & name);

int main()
{
	const string filename = "names.txt"; // file name with the names to be put into the tree
	
	BinarySearchTree<string> tree;		// our binary search tree

	fileToTree(tree, filename);			// get the names from the file to the tree
	
	// Print the Tree Diagram for the User
	cout << "     Tree Diagram :\n\n";
	tree.BSTPrint();

	cout << "\n\n\n";
	system("pause");

	// Print the contents in order
	void (*fptr)(string&);
	fptr = visit;
	cout << "\n\n\nIn order contents of tree :\n";
	tree.inorderTraverse(fptr);

	cout << "\n\n\n";
	system("pause");

	// Search for a name that is in there and one that is not
	string name1 = "Nancy Drew";
	string name2 = "Joey Saldano";

	string temp = (tree.contains(name1))? " is " : " is not ";
		cout << name1 << temp << "contained in the tree. " << endl;
	 temp = (tree.contains(name2))? " is " : " is not ";
		cout << name2 << temp << "contained in the tree. " << endl;

		cout << "\n\n\n";
	system("pause");

}

template<class ItemType>
void fileToTree(BinarySearchTree<ItemType> & temp, string filename)
{
	string STRING;
	ifstream infile;
	infile.open(filename);
	while(!infile.eof()) // To get you all the lines.
    {
	    getline(infile,STRING); // Saves the line in STRING.
	    temp.add(STRING);
    }
	infile.close();
}

void visit(string & name)
{
	static int count = 1;
	cout << count << " : " << name << endl;
	count++;
}